import datetime
import time

import uhd

device = uhd.usrp.MultiUSRP()

device.set_time_source("external")
device.set_clock_source("external")

print("*"*80)
print("*"*80)
print(device.get_mboard_name())

time_spec = uhd.types.TimeSpec.from_ticks(0, 1.0)
device.set_time_next_pps(time_spec)

time.sleep(5)

for i in range(1):
    now1 = datetime.datetime.now()
    uhd_now = device.get_time_now()
    now2 = datetime.datetime.now()
    print(f"Local now: {now1}")
    print(f"Local now: {now2}")
    print(f"UHD time: {uhd_now.get_frac_secs()}")
    print(f"UHD time: {uhd_now.get_full_secs()}")
    offset = 1 + now1.microsecond / 1e6 - uhd_now.get_frac_secs()
    while offset > 0.5:
        offset -= 1
    print(f"now1 - UHD time %1: {offset}")
    time.sleep(1.1)

